$(function () {

    $(window).on('resize', function () {
            var h = $(document).height(),
                w = $(document).width();

            /*  Math.atan() function returns the arctangent (in radians) 
             *  of a number and 1 rad ~= 57.29577 deg 
             */
            var angle = Math.atan(h / w) * 57.29577;
            var rotateProperty = "rotate(" + angle + "deg)";

            $('section').css({
                "-webkit-transform": rotateProperty,
                "-moz-transform": rotateProperty,
                "transform": rotateProperty
            });

        })
        .triggerHandler('resize');
});